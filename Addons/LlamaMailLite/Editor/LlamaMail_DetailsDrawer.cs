﻿using UnityEditor;
using UnityEngine;
using LlamaMail;
using System;

[CustomPropertyDrawer(typeof(LlamaMail_details))]
public class LlamaMail_DetailsDrawer : PropertyDrawer {
	private const float ROW_HEIGHT = 16f;
	private const float PADDING = 4f;

	private SerializedProperty expiresAmount;
	private SerializedProperty expiresPart;

	private void FetchSerializedProperties(SerializedProperty property) {
		//if not set yet, set them all
		if (expiresAmount == null || expiresPart == null) {
			expiresAmount = property.FindPropertyRelative("expiresAmount");
			expiresPart = property.FindPropertyRelative("expiresPart");
		}
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		FetchSerializedProperties(property);

		position.y += PADDING;

		//draw border
		//background
		EditorGUI.DrawRect(new Rect(position.x, position.y, position.width, position.height - 6f), new Color(.5f, .7f, .9f));
		//top
		EditorGUI.DrawRect(new Rect(position.x, position.y, position.width, 2f), Color.black);
		//left
		EditorGUI.DrawRect(new Rect(position.x, position.y, 2f, position.height - 6f), Color.black);
		//right
		EditorGUI.DrawRect(new Rect(position.x + position.width, position.y, 2f, position.height - 6f), Color.black);
		//bottom
		EditorGUI.DrawRect(new Rect(position.x, position.y + position.height - 8f, position.width, 2f), Color.black);

		position.x += PADDING;
		position.y += PADDING;

		GUIStyle headerStyle = new GUIStyle();
		headerStyle.fontSize = 16;
		headerStyle.fontStyle = FontStyle.Bold;

		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		Rect rowPos = new Rect(position.x, position.y, position.width - PADDING * 2, ROW_HEIGHT);

		// Draw Header
		EditorGUI.PrefixLabel(rowPos, GUIUtility.GetControlID(FocusType.Passive), new GUIContent("Llama Mail (Lite)", "View all Llama add-ons at: https://jkuhn.com"), headerStyle);

		//move down to next label
		rowPos.y += ROW_HEIGHT + 6f; //bump down a little extra to make space for larger header

		Rect propPos = EditorGUI.PrefixLabel(rowPos, GUIUtility.GetControlID(FocusType.Passive), new GUIContent("Mail Expires After"));

		Rect amtPos = new Rect(propPos.x, propPos.y, propPos.width - 102f, propPos.height);
		Rect intPos = new Rect(propPos.x + propPos.width - 100f, propPos.y, 100f, propPos.height);

		EditorGUI.BeginChangeCheck();
		int amt = expiresAmount.intValue;
		DateInterval expiresInterval = (DateInterval)expiresPart.enumValueIndex;

		int.TryParse(EditorGUI.TextField(amtPos, amt.ToString()), out amt);
		Enum newValue = EditorGUI.EnumPopup(intPos, expiresInterval);

		if (EditorGUI.EndChangeCheck()) {
			expiresAmount.intValue = amt;
			expiresInterval = (DateInterval)newValue;
			expiresPart.enumValueIndex = (int)expiresInterval;
		}

		EditorGUI.indentLevel = indent;

	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		FetchSerializedProperties(property);
		int rowCount = 2;

		//add some for expiration settings and interval
		rowCount ++;

		return (ROW_HEIGHT * rowCount) + (PADDING * 2) + 4;
	}
}