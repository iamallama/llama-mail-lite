﻿// Note: this script has to be on an always-active UI parent, so that we can
// always find it from other code. (GameObject.Find doesn't find inactive ones)
using UnityEngine;
using UnityEngine.UI;
using LlamaMail;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine.Events;

public partial class UIMailMessage : MonoBehaviour {
	public NetworkManagerMMO manager; // singleton is null until update

	public GameObject panel;

	public string recipient;

	public RectTransform searchPanel;
	public InputField search;
	public RectTransform searchContent;
	public UIMailSearchSlot searchSlot;
	public Button searchButton;

	public RectTransform messagePanel;
	public Text recipientText;
	public InputField subject;
	public InputField body;

	public Text dialogMessage;
	public GameObject dialog;
	public Button dialogButton;

	public Button acceptButton;
	public Button cancelButton;

	private bool sending = false;

	void Update() {
		var player = Utils.ClientLocalPlayer();
		if (!player) return;

		if (manager == null) {
			manager = FindObjectOfType<NetworkManagerMMO>();
		}

		if (manager != null && panel.activeSelf) {
			//no one selected yet, show search box
			if (string.IsNullOrEmpty(recipient)) {
				searchPanel.gameObject.SetActive(true);
				messagePanel.gameObject.SetActive(false);

				if (search.text.Length > 0 &&
					search.text.Length <= manager.accountMaxLength &&
					Regex.IsMatch(search.text, @"^[a-zA-Z0-9_]+$")) {
					searchButton.interactable = true;
				} else {
					searchButton.interactable = false;
				}
				searchButton.onClick.SetListener(() => {
					if (Time.time >= player.nextRiskyActionTime) {
						player.nextRiskyActionTime = Time.time + player.mailWaitSeconds;
						//prepare and send search request, get response
						sending = true;
						search.interactable = false;
						searchButton.interactable = false;
						player.CmdMail_Search(search.text);
						dialogMessage.text = "Searching...";
						dialog.SetActive(true);
					}
				});
			} else {
				searchPanel.gameObject.SetActive(false);
				messagePanel.gameObject.SetActive(true);
				recipientText.text = recipient;

				acceptButton.interactable = !string.IsNullOrEmpty(subject.text);
				acceptButton.onClick.SetListener(() => {
					if(Time.time >= player.nextRiskyActionTime) {
						player.nextRiskyActionTime = Time.time + player.mailWaitSeconds;
						sending = true;
						dialogMessage.text = "Sending Mail...";
						dialog.SetActive(true);

						player.CmdMail_Send(recipient, subject.text, body.text);
					}
				});

				dialogButton.onClick.SetListener(() => {
					cancelButton.onClick.Invoke();
				});
			}

			//show the dialog button if we are not sending
			dialogButton.gameObject.SetActive(!sending);

			// cancel
			cancelButton.interactable = !sending;
			cancelButton.onClick.SetListener(() => {
				recipient = "";
				search.text = "";
				subject.text = "";
				body.text = "";
				sending = false;
				UIUtils.BalancePrefabs(searchSlot.gameObject, 0, searchContent);
				dialog.SetActive(false);
				panel.SetActive(false);
			});
		}

		// addon system hooks
		Utils.InvokeMany(typeof(UIMailMessage), this, "Update_");
	}

	public void UpdateSearchResults(List<MailSearch> results) {
		UIUtils.BalancePrefabs(searchSlot.gameObject, results.Count, searchContent);
		for (int i = 0; i < results.Count; ++i) {
			var slot = searchContent.GetChild(i).GetComponent<UIMailSearchSlot>();
			slot.nameText.text = results[i].name;
			slot.levelText.text = results[i].level.ToString();
			slot.guildText.text = results[i].guild == null ? "" : results[i].guild;
			slot.actionButton.onClick.SetListener(() => {
				recipient = slot.nameText.text;
			});
		}

		searchButton.interactable = true;
		search.interactable = true;
		sending = false;
		dialog.SetActive(false);
	}

	public void MailMessageSent(string status) {
		dialogMessage.text = status;
		sending = false;
		dialog.SetActive(true);
	}
}
