﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using LlamaMail;
using Mono.Data.Sqlite;
using System;

/**
 *	TODO:
 *		-Need to set all actions to use safe timing
 */

public partial class Npc {
	[Header("Llama Mail Inbox")]
	public bool offersMailSend = true;
}

public partial class UINpcDialogue {
	public GameObject npcMailSendPanel;
	public Button npcMailSendButton;

	public void Update_Mail() {
		var player = Utils.ClientLocalPlayer();
		if (!player) return;

		// use collider point(s) to also work with big entities
		if (panel.activeSelf &&
			player.target != null && player.target is Npc &&
			Utils.ClosestDistance(player.collider, player.target.collider) <= player.interactionRange) {
			var npc = (Npc)player.target;

			npcMailSendButton.gameObject.SetActive(npc.offersMailSend);
			npcMailSendButton.onClick.SetListener(() => {
				npcMailSendPanel.SetActive(true);
				panel.SetActive(false);
			});
		}
	}
}

public partial class UIShortcuts : MonoBehaviour {
	public Button mailInboxButton;
	public GameObject mailInboxPanel;

	public void Update_Mail() {
		mailInboxButton.onClick.SetListener(() => {
			mailInboxPanel.SetActive(!mailInboxPanel.activeSelf);
		});

	}
}


public partial class Player : Entity {
	public SyncListMailMessage llamaMailMessages = new SyncListMailMessage();

	public LlamaMail_details llamaMail = new LlamaMail_details();

	public float mailWaitSeconds = 3;

	[Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
	public void CmdMail_Send(string messageTo, string subject, string body) {
		//validate the message

		string errors = "";

		if (string.IsNullOrEmpty(messageTo)) {
			errors += "Need recipient\n";
		}
		if (string.IsNullOrEmpty(subject)) {
			errors += "Subject is required\n";
		}

		//if no errors yet, perform more complicated checks
		if (string.IsNullOrEmpty(errors)) {

			long expiration = Mail_CalculateExpiration();
			Database.Mail_CreateMessage(name, messageTo, subject, body, expiration);
			//commit player immediately so if server went offline, any items/gold are not recovered
			Database.CharacterSave(this, true);

			TargetMail_SendResults(connectionToClient, "Mail Sent", true);
		} else {
			TargetMail_SendResults(connectionToClient, errors, false);
		}

	}

	public long Mail_CalculateExpiration() {
		long expiration = 0;
		switch (llamaMail.expiresPart) {
			case DateInterval.Seconds:
				expiration = llamaMail.expiresAmount;
				break;
			case DateInterval.Minutes:
				expiration = llamaMail.expiresAmount * 60;
				break;
			case DateInterval.Hours:
				expiration = llamaMail.expiresAmount * 3600;
				break;
			case DateInterval.Days:
				expiration = llamaMail.expiresAmount * 86400;
				break;
			case DateInterval.Months:
				expiration = llamaMail.expiresAmount * 86400 * 30;
				break;
			case DateInterval.Years:
				expiration = llamaMail.expiresAmount * 86400 * 365;
				break;

		}

		//convert to milliseconds
		return expiration * 1000;
	}

	[TargetRpc(channel = Channels.DefaultUnreliable)] // only send to one client
	public void TargetMail_SendResults(NetworkConnection target, string status, bool success) {
		var mail = FindObjectOfType<UIMailMessage>();
		
		if(mail != null) {
			mail.MailMessageSent(status);
		}
	}

	[Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
	public void CmdMail_Search(string searchString) {
		List<MailSearch> result = Database.Mail_SearchForCharacter(searchString, name);
		//serialize the result to string (easiest)
		string[] serialized = new string[result.Count];
		for(int i=0; i<result.Count; i++) {
			serialized[i] = result[i].name + "|" + result[i].level + "|";
			if(result[i].guild != null) {
				serialized[i] += result[i].guild;
			}
		}

		TargetMail_SearchResults(connectionToClient, String.Join("&", serialized));
	}

	[TargetRpc(channel = Channels.DefaultUnreliable)] // only send to one client
	public void TargetMail_SearchResults(NetworkConnection target, string searchResults) {
		var mail = FindObjectOfType<UIMailMessage>();
		List<MailSearch> results = new List<MailSearch>();

		if (!string.IsNullOrEmpty(searchResults)) {
			string[] tmp = searchResults.Split('&');

			for(var i=0; i<tmp.Length; i++) {
				string[] tmp2 = tmp[i].Split('|');

				MailSearch res = new MailSearch();
				res.name = tmp2[0];
				int.TryParse(tmp2[1], out res.level);
				res.guild = tmp2[2];
				results.Add(res);
			}
		}

		if(mail != null) {
			mail.UpdateSearchResults(results);
		}
	}

	[Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
	public void CmdMail_ReadMessage(int index) {
		if(index >= 0 && index < llamaMailMessages.Count) {
			LlamaMailMessage message = llamaMailMessages[index];
			message.read = Epoch.Current();
			llamaMailMessages[index] = message;
			Database.Mail_UpdateMessage(message);
		}
	}

	[Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
	public void CmdMail_DeleteMessage(int index) {
		if (index >= 0 && index < llamaMailMessages.Count) {
			LlamaMailMessage message = llamaMailMessages[index];
			message.deleted = Epoch.Current();
			llamaMailMessages.RemoveAt(index);
			Database.Mail_UpdateMessage(message);
		}
	}
}

public partial class Database {

	public static void Initialize_Mail() {
		ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS mail (
							id INTEGER PRIMARY KEY,
							messageFrom TEXT NOT NULL,
							messageTo TEXT NOT NULL,
							subject TEXT NOT NULL,
							body TEXT NOT NULL,
							sent INTEGER NOT NULL,
							expires INTEGER NOT NULL,
							read INTEGER NOT NULL,
							deleted INTEGER NOT NULL)");
	}

	public static LlamaMailMessage Mail_BuildMessageFromDBRow(List<object> row) {
		LlamaMailMessage message = new LlamaMailMessage();

		int colNum = 0;
		message.id		= (long)row[colNum++];
		message.from	= (string)row[colNum++];
		message.to		= (string)row[colNum++];
		message.subject = (string)row[colNum++];
		message.body	= (string)row[colNum++];
		message.sent	= (long)row[colNum++];
		message.expires = (long)row[colNum++];
		message.read	= (long)row[colNum++];
		message.deleted = (long)row[colNum++];

		return message;
	}

	public static void CharacterLoad_Mail(Player player) {
		Debug.Log("Loading mail for " + player.name);

		var table = ExecuteReader("SELECT * FROM mail WHERE messageTo=@character AND deleted=0 AND expires > @expires ORDER BY sent", new SqliteParameter("@character", player.name), new SqliteParameter("@expires", Epoch.Current()));
		foreach (var row in table) {
			LlamaMailMessage message = Mail_BuildMessageFromDBRow(row);
			player.llamaMailMessages.Add(message);
		}

		Debug.Log(player.name + " has " + player.llamaMailMessages.Count + " messages.");
	}

	public static List<MailSearch> Mail_SearchForCharacter(string name, string selfPlayer) {
		List<MailSearch> result = new List<MailSearch>();

		/**
		 * Order by here is setup in such a way that:
		 *		exact matches appear first
		 *		followed by names where the search string is closer to the front of the name
		 */
		var table = ExecuteReader(@"SELECT name, level, IFNULL(guild, '')
									FROM characters
										LEFT JOIN guild_members
											ON character=name
									WHERE name LIKE '%' || @search || '%'
										AND name <> @self
									ORDER BY
										CASE
											WHEN name=@search THEN 0
											ELSE INSTR(LOWER(name), LOWER(@search))
										END, name
									LIMIT 30", new SqliteParameter("@search", name), new SqliteParameter("@self", selfPlayer));
		foreach (var row in table) {
			var res = new MailSearch();
			res.name = (string)row[0];
			res.level = Convert.ToInt32((long)row[1]);
			res.guild = (string)row[2];

			result.Add(res);
		}

		return result;
	}

	public static void Mail_CreateMessage(string from, string to, string subject, string body="", long expiration=0) {
		long sent = Epoch.Current();
		long expires = 0;
		if(expiration > 0) {
			expires = sent + expiration;
		}

		ExecuteNonQuery(@"INSERT INTO mail (
							messageFrom, messageTo, subject, body, sent, expires, read, deleted
						) VALUES (
							@from, @to, @subject, @body, @sent, @expires, 0, 0
						)",
						new SqliteParameter("@from", from),
						new SqliteParameter("@to", to),
						new SqliteParameter("@subject", subject),
						new SqliteParameter("@body", body),
						new SqliteParameter("@sent", sent),
						new SqliteParameter("@expires", expires)
						);
	}

	public static void Mail_UpdateMessage(LlamaMailMessage message) {
		//only 2 parts that need to be updated
		ExecuteNonQuery(@"UPDATE mail SET
							read=@read,
							deleted=@deleted
						WHERE id=@id",
						new SqliteParameter("@read", message.read),
						new SqliteParameter("@deleted", message.deleted),
						new SqliteParameter("@id", message.id));
	}

	public static LlamaMailMessage Mail_MessageById(long id) {
		LlamaMailMessage message = new LlamaMailMessage();

		var table = ExecuteReader("SELECT * FROM mail WHERE id=@id", new SqliteParameter("@id", id));
		if(table.Count == 1) {
			message = Mail_BuildMessageFromDBRow(table[0]);
		}

		return message;
	}

	public static List<LlamaMailMessage> Mail_CheckForNewMessages(long maxID) {
		List<LlamaMailMessage> result = new List<LlamaMailMessage>();

		var table = ExecuteReader("SELECT * FROM mail WHERE id > @maxid AND deleted=0 AND expires > @expires ORDER BY sent", new SqliteParameter("@maxid", maxID), new SqliteParameter("@expires", Epoch.Current()));
		foreach (var row in table) {
			LlamaMailMessage message = Mail_BuildMessageFromDBRow(row);
			result.Add(message);
		}

		return result;
	}
}

public partial class NetworkManagerMMO : NetworkManager {

	public void OnStartServer_MailChecker() {

		StartCoroutine(UpdateMailStatus());
	}

	private IEnumerator UpdateMailStatus() {
		yield return null;

		//get the last known ID known on server startup
		//new messages are considered to be any after this point so we can notify people of new messages
		long maxID = (long)Database.ExecuteScalar("SELECT IFNULL(id, 0) FROM (SELECT MAX(id) AS id FROM mail)");

		while (true) {
			//delay interval
			yield return new WaitForSeconds(20f);

			//check for new messages
			List<LlamaMailMessage> newMessages = Database.Mail_CheckForNewMessages(maxID);
			Debug.Log("Checking for new mail. " + newMessages.Count.ToString() + " new messages found.");

			foreach (LlamaMailMessage message in newMessages) {
				//if the player is online, add to their synclist
				if (Player.onlinePlayers.ContainsKey(message.to)) {
					Player.onlinePlayers[message.to].llamaMailMessages.Add(message);
				}

				if(message.id > maxID) {
					maxID = message.id;
				}
			}
		}
	}

}
