﻿namespace LlamaMail {

	using UnityEngine;

	public enum DateInterval {
		Years,
		Months,
		Weeks,
		Days,
		Hours,
		Minutes,
		Seconds
	}

	[System.Serializable]
	public class LlamaMail_details {
		public int expiresAmount = 30;
		public DateInterval expiresPart = DateInterval.Days;
	}
}