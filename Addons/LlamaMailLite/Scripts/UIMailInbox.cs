﻿// Note: this script has to be on an always-active UI parent, so that we can
// always find it from other code. (GameObject.Find doesn't find inactive ones)
using UnityEngine;
using UnityEngine.UI;
using LlamaMail;
using System.Collections.Generic;
using System.Linq;

public partial class UIMailInbox : MonoBehaviour {
	public RectTransform messagesContent;
	public RectTransform readContent;
	public GameObject messageSlot;
	public Text receivedText;
	public Text expiresText;
	public Text fromText;
	public Text subjectText;
	public Text bodyText;

	private int readingIndex = -1;

	private int cnt = 0;

	void Update() {
		Player player = Utils.ClientLocalPlayer();
		if (player == null) return;

		long current = Epoch.Current();

		//count messages that haven't expired yet
		int mailCount = player.llamaMailMessages.Count((m) => current <= m.expires);
		UIUtils.BalancePrefabs(messageSlot, mailCount, messagesContent);

		if(mailCount != cnt) {

			cnt = mailCount;
		}

		int slotIndex = -1;

		//loop over messages backwards because we add to the synclist so newer messages appear at end, we want to display newer on top
		for(var mailIndex=player.llamaMailMessages.Count-1; mailIndex >=0; mailIndex--) {
			LlamaMailMessage message = player.llamaMailMessages[mailIndex];
			//if message has expired, skip it
			if (current > message.expires) continue;

			slotIndex++;
			var slot = messagesContent.GetChild(slotIndex).GetComponent<UIMailMessageSlot>();
			slot.textReceived.text = message.sentAt;
			slot.textFrom.text = message.from;
			slot.textSubject.text = message.subject;

			//if the message has been read, show normal font, else bold text
			if (message.read > 0) {
				slot.textReceived.fontStyle = FontStyle.Normal;
				slot.textFrom.fontStyle = FontStyle.Normal;
				slot.textSubject.fontStyle = FontStyle.Normal;
			} else {
				slot.textReceived.fontStyle = FontStyle.Bold;
				slot.textFrom.fontStyle = FontStyle.Bold;
				slot.textSubject.fontStyle = FontStyle.Bold;
			}

			int tmpIndex = mailIndex;

			//click on slot, read message
			slot.readButton.onClick.SetListener(() => {
				readingIndex = tmpIndex;
				if(message.read == 0) {
					player.CmdMail_ReadMessage(tmpIndex);
				}
			});

			//setup delete button
			slot.deleteButton.onClick.SetListener(() => {
				if(Time.time >= player.nextRiskyActionTime) {
					player.nextRiskyActionTime = Time.time + player.mailWaitSeconds;
					//on delete clear the currently reading message
					readingIndex = -1;
					player.CmdMail_DeleteMessage(tmpIndex);
				}
			});
		}

		if (readingIndex > -1) {
			LlamaMailMessage reading = player.llamaMailMessages[readingIndex];

			readContent.gameObject.SetActive(true);
			receivedText.text = reading.sentAt;
			expiresText.text = reading.expiresAt;
			fromText.text = reading.from;
			subjectText.text = reading.subject;
			bodyText.text = reading.body;
		} else {
			readContent.gameObject.SetActive(false);
		}
	}
}
