﻿// Attach to the prefab for easier component access by the UI Scripts.
// Otherwise we would need slot.GetChild(0).GetComponentInChildren<Text> etc.
using UnityEngine;
using UnityEngine.UI;

public class UIMailMessageSlot : MonoBehaviour {
	public Text textReceived;
	public Text textFrom;
	public Text textSubject;
	public Button readButton;
	public Button deleteButton;
}
