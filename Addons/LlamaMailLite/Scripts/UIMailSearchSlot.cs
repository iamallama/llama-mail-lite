﻿// Attach to the prefab for easier component access by the UI Scripts.
// Otherwise we would need slot.GetChild(0).GetComponentInChildren<Text> etc.
using UnityEngine;
using UnityEngine.UI;

public class UIMailSearchSlot : MonoBehaviour {
    public Image icon;
    public Text nameText;
    public Text levelText;
    public Text guildText;
    public Button actionButton;
}
